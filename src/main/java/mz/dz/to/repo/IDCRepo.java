package mz.dz.to.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import mz.dz.to.entities.DetailsCommande;

public interface IDCRepo extends JpaRepository<DetailsCommande, Long>{

}
