package mz.dz.to.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mz.dz.to.entities.Genre;

@Repository
public interface IGenRepo extends JpaRepository<Genre, Long> {
	@Query("SELECT g FROM Genre g WHERE g.genre = ?1")
	public Genre findByGen(String genre);
}
