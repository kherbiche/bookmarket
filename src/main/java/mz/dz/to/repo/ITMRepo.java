package mz.dz.to.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mz.dz.to.entities.TypeMedia;

public interface ITMRepo extends JpaRepository<TypeMedia, Long> {
	@Query("SELECT t FROM TypeMedia t WHERE t.media = ?1")
	public TypeMedia findByMed(String media);
}