package mz.dz.to.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import mz.dz.to.entities.Commande;

public interface IComRepo extends JpaRepository<Commande, Long> {
}
