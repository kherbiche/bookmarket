package mz.dz.to.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mz.dz.to.entities.Editeur;

@Repository
public interface IEditeurRepo extends JpaRepository<Editeur, Long> {
	@Query("SELECT e FROM Editeur e WHERE e.siteWeb = ?1")
	public Editeur findBySite(String siteWeb);
}
