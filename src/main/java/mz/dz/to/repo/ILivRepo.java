package mz.dz.to.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mz.dz.to.entities.Livre;

public interface ILivRepo extends JpaRepository<Livre, Long> {
	@Query(value ="SELECT * FROM LIVRE l WHERE l.isbn=?1",nativeQuery=true)
	public Livre findByITPD(String i, String t, String p, String d);
}
