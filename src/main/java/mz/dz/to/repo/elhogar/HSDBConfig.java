package mz.dz.to.repo.elhogar;

import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "hsdbEntityManagerFactory", transactionManagerRef = "hsdbTransactionManager", basePackages = "mz.dz.to.repo")
public class HSDBConfig {
	@Bean("hsds")
	@ConfigurationProperties(prefix = "spring.hs.datasource")
	public DataSource hsDataSource() {
		return DataSourceBuilder.create().build();
	}
	@SuppressWarnings("unchecked")
	@Bean(name = "hsdbEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean hsdbEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder
					.dataSource(hsDataSource())
					.properties(hibernateProperties())
					.packages("mz.dz.to.entities")
					.persistenceUnit("hsdbPU")
					.build();
	}
	@Bean(name = "hsdbTransactionManager")
	public PlatformTransactionManager hsdbTransactionManager(@Qualifier("hsdbEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
	@SuppressWarnings("rawtypes")
	private Map hibernateProperties() {
		Properties properties = new Properties() {
			private static final long serialVersionUID = 1L;
			{
				setProperty("hibernate.hbm2ddl.auto", "create-drop");
				setProperty("hibernate.show_sql", "true");
				setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
			}
		};
		return properties.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue()));
	}
}
