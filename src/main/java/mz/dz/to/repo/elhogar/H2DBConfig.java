package mz.dz.to.repo.elhogar;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "h2dbEntityManagerFactory", transactionManagerRef = "h2dbTransactionManager", basePackages = "mz.dz.to.repo")
public class H2DBConfig {
	private static final Logger logger = LoggerFactory.getLogger(H2DBConfig.class);
	@Bean("dataSource")
	@ConfigurationProperties(prefix = "spring.h2db.datasource")
	public DataSource h2DataSource() {
		return DataSourceBuilder.create().build();
	}
	@Bean
	public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
	   return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), new HashMap<>(), null);
	}
	@SuppressWarnings("unchecked")
	@Bean(name = "h2dbEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean h2dbEntityManagerFactory(EntityManagerFactoryBuilder builder) {  
		return builder
				.dataSource(h2DataSource())
				.properties(hibernateProperties())
				.packages("mz.dz.to.entities")
				.persistenceUnit("h2dbPU")
				.build();
	}
	@Bean(name = "h2dbTransactionManager")
	public PlatformTransactionManager h2dbTransactionManager(@Qualifier("h2dbEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
	@SuppressWarnings("rawtypes")
	private Map hibernateProperties() {
		Properties properties = new Properties() {
			private static final long serialVersionUID = 1L;
			{
				setProperty("hibernate.hbm2ddl.auto", "create-drop");
				setProperty("hibernate.show_sql", "true");
				setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
			}
		};
		return properties.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue()));
	}
	//////////////////////////////
	//
	// This method called after
	// all beans initialization
	//
	//////////////////////////////
	@EventListener
    private void onApplicationEvent(ContextRefreshedEvent event) throws IOException {
        logger.info("Populating H2DB from sql script ...");
	    ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(new ClassPathResource("pop.sql"));
	    databasePopulator.setContinueOnError(true);
	    databasePopulator.execute(h2DataSource());
	    logger.info("H2DB populated!!");
    }
}
