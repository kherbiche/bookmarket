////////////////////////////////////////////////////////////////////////////////////
//
// This following class utilizes an EntityMangerFactory, EntityManger, ThreadLocal
// to manage persistence and transaction in the application for both 
// persistence unit.
//
// The EMUtils program needs the EntityMangerFactory bean, in order to create
// EntityManager instance.
//
// Method syntax:
//     EntityManager getEm(String url)
//
////////////////////////////////////////////////////////////////////////////////////
package mz.dz.to.repo.elhogar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.stereotype.Component;

import mz.dz.to.repo.IAutRepo;
import mz.dz.to.repo.IClientRepo;
import mz.dz.to.repo.IComRepo;
import mz.dz.to.repo.IDCRepo;
import mz.dz.to.repo.IEditeurRepo;
import mz.dz.to.repo.IGenRepo;
import mz.dz.to.repo.ILivRepo;
import mz.dz.to.repo.ITMRepo;

/**
 * 
 * @author Lyes KHERBICHE
 */
@Component
public class EMUtils {
	private static final Logger logger = LoggerFactory.getLogger(EMUtils.class);
	@Autowired
	@Qualifier("h2dbEntityManagerFactory")
	private EntityManagerFactory emfh2;
	@Autowired
	@Qualifier("hsdbEntityManagerFactory")
	private EntityManagerFactory emfhs;
	private static final ThreadLocal<EntityManager> threadLocalEnityManagerH2, threadLocalEnityManagerHS;
	private EntityManager emh2, emhs;
	static {
		threadLocalEnityManagerH2 = new ThreadLocal<EntityManager>();
		threadLocalEnityManagerHS = new ThreadLocal<EntityManager>();
	}
	public EntityManager getEm(String url) {
		if (url.contains("h2")) {
			logger.info("getEm(String url): " + url);
			emh2 = threadLocalEnityManagerH2.get();
			if(emh2 == null) {
				logger.info("EntityManager H2 == null");
				emh2 = emfh2.createEntityManager();
				threadLocalEnityManagerH2.set(emh2);
			}
			return emh2;
		}
		if (url.contains("hs")) {
			logger.info("getEm(String url): " + url);
			emhs = threadLocalEnityManagerHS.get();
			if(emhs == null) {
				logger.info("EntityManager HS == null");
				emhs = emfhs.createEntityManager();
				threadLocalEnityManagerHS.set(emhs);
			}
			return emhs;
		}
		return null;
	}
	public IClientRepo getClientRepo(String url){
	        return new JpaRepositoryFactory(getEm(url)).getRepository(IClientRepo.class);
	}
	public IEditeurRepo getEditRepo(String url){
        return new JpaRepositoryFactory(getEm(url)).getRepository(IEditeurRepo.class);
	}
	public IGenRepo getGenRepo(String url){
        return new JpaRepositoryFactory(getEm(url)).getRepository(IGenRepo.class);
	}
	public ITMRepo getTMRepo(String url){
        return new JpaRepositoryFactory(getEm(url)).getRepository(ITMRepo.class);
	}
	public IAutRepo getAutRepo(String url){
        return new JpaRepositoryFactory(getEm(url)).getRepository(IAutRepo.class);
	}
	public ILivRepo getLivRepo(String url){
        return new JpaRepositoryFactory(getEm(url)).getRepository(ILivRepo.class);
	}
	public IComRepo getComRepo(String url){
        return new JpaRepositoryFactory(getEm(url)).getRepository(IComRepo.class);
	}
	public IDCRepo getIDCRepo(String url){
        return new JpaRepositoryFactory(getEm(url)).getRepository(IDCRepo.class);
	}
}
