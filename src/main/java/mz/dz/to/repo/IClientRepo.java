package mz.dz.to.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mz.dz.to.entities.Client;

@Repository
public interface IClientRepo extends JpaRepository<Client, Long> {
	@Query("SELECT c FROM Client c WHERE c.zipCodePostal = ?1")
	public Client findByZip(String zipCodePostal);
}
