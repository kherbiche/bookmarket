package mz.dz.to.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mz.dz.to.entities.Auteur;

public interface IAutRepo extends JpaRepository<Auteur, Long> {
	@Query(value ="SELECT * FROM AUTEUR a WHERE a.nomFamille = ?1 and a.prenom = ?2", nativeQuery = true)
	public Auteur findByNP(String nom, String prenom);
}
