package mz.dz.to.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mz.dz.to.entities.Client;
import mz.dz.to.repo.elhogar.EMUtils;
import mz.dz.to.web.CtrlHelper;

@Service
public class SeltanMejbada {
	private static final Logger logger = LoggerFactory.getLogger(SeltanMejbada.class);
	@Autowired
	private EMUtils emutils ;
	
	public void createClient(Client client) {
		logger.info("ClientService.save(client) hs");
		emutils.getEm("hs").getTransaction().begin();
		emutils.getClientRepo("hs").save(client);
		emutils.getEm("hs").getTransaction().commit();
		
		logger.info("ClientService.save(client) h2");
		emutils.getEm("h2").getTransaction().begin();
		emutils.getClientRepo("h2").save(client);
		emutils.getEm("h2").getTransaction().commit();
	}
	public void process(CtrlHelper helper) {
		/**
		 * logger.info("process(helper)");
		 * logger.info("id client: "+emutils.getClientRepo("h2").findByZip(helper.getClient().getZipCodePostal()).getId());
		 * logger.info("id editeur: "+emutils.getEditRepo("h2").findBySite(helper.getEditeur().getSiteWeb()).getId());
		 * logger.info("id genre: "+emutils.getGenRepo("h2").findByGen(helper.getGenre().getGenre()).getId());
		 * logger.info("id TypeMedia: "+emutils.getTMRepo("h2").findByMed(helper.getTypeMedia().getMedia()).getId());
		 * logger.info("id Auteur: "+emutils.getAutRepo("h2").findByNP(helper.getAuteur().getNomFamille(), helper.getAuteur().getPrenom()).getId());
		 * logger.info("id Livre: "+emutils.getLivRepo("h2").findByITPD(helper.getLivre().getIsbn(), helper.getLivre().getTitre(), 
				helper.getLivre().getPrix(), helper.getLivre().getDatePublication()).getId());
		 * logger.info("id client: "+helper.getClient().getId());
		 */
		//setting @Client to @Commande object
		helper.getCommande().setClient(emutils.getClientRepo("h2").findByZip(helper.getClient().getZipCodePostal()));
		//setting @Livre, @TypeMedia, to detailsCommande object
		helper.getDetailsCommande().setLivre(emutils.getLivRepo("h2").findByITPD(helper.getLivre().getIsbn(), helper.getLivre().getTitre(), 
				helper.getLivre().getPrix(), helper.getLivre().getDatePublication()));
		helper.getDetailsCommande().setTypeMedia(emutils.getTMRepo("h2").findByMed(helper.getTypeMedia().getMedia()));
		
		emutils.getEm("h2").getTransaction().begin();
		helper.getDetailsCommande().setCommande(emutils.getComRepo("h2").save(helper.getCommande()));
		helper.getDetailsCommande().getId().setId_commande(helper.getDetailsCommande().getCommande().getId());
		helper.getDetailsCommande().getId().setId_livre(helper.getDetailsCommande().getLivre().getId());
		emutils.getIDCRepo("h2").save(helper.getDetailsCommande());
		emutils.getEm("h2").getTransaction().commit();
	}
}