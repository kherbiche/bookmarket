package mz.dz.to;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

//@EnableJpaRepositories("mz.dz.to.repo")
@EntityScan("mz.dz.to.entities")
@ComponentScan({"mz.dz.to*"})
@SpringBootApplication
public class App {

	public static void main(String[] args){
		SpringApplication.run(App.class, args);
	}
}
