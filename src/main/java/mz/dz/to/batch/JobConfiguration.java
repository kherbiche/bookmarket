package mz.dz.to.batch;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.support.SimpleFlow;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.H2PagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

import mz.dz.to.entities.Client;
import mz.dz.to.entities.Editeur;
import mz.dz.to.entities.Genre;
import mz.dz.to.entities.TypeMedia;

@Configuration
@EnableBatchProcessing
public class JobConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(JobConfiguration.class);
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	@Autowired
	@Qualifier("hsds")
	private DataSource hsds;

	@Bean
	public JdbcCursorItemReader<Client> clientReader() {
		JdbcCursorItemReader<Client> reader = new JdbcCursorItemReader<>();
		reader.setSql("SELECT * FROM CLIENT ORDER BY NOMFAMILLE, PRENOM");
		reader.setDataSource(dataSource);
		reader.setFetchSize(100);
		reader.setRowMapper(new ClientRowMapper());
		return reader;
	}
	@Bean
	public JdbcCursorItemReader<Genre> genreReader() {
		JdbcCursorItemReader<Genre> reader = new JdbcCursorItemReader<>();
		reader.setSql("SELECT * FROM GENRE");
		reader.setDataSource(dataSource);
		reader.setFetchSize(100);
		reader.setRowMapper(new GenreRowMapper());
		return reader;
	}
	@Bean
	public JdbcCursorItemReader<TypeMedia> mediaReader() {
		JdbcCursorItemReader<TypeMedia> reader = new JdbcCursorItemReader<>();
		reader.setSql("SELECT * FROM TYPEMEDIA");
		reader.setDataSource(dataSource);
		reader.setFetchSize(100);
		reader.setRowMapper(new TypeMediaRowMapper());
		return reader;
	}
	@Bean
	public JdbcCursorItemReader<Editeur> editeurReader() {
		JdbcCursorItemReader<Editeur> reader = new JdbcCursorItemReader<>();
		reader.setSql("SELECT * FROM EDITEUR");
		reader.setDataSource(dataSource);
		reader.setFetchSize(100);
		reader.setRowMapper(new EditeurRowMapper());
		return reader;
	}
	// This is Thread-safe
	@Bean
	public JdbcPagingItemReader<Client> pagingItemReader() {
		JdbcPagingItemReader<Client> reader = new JdbcPagingItemReader<>();
		reader.setDataSource(dataSource);
		reader.setFetchSize(10);
		reader.setRowMapper(new ClientRowMapper());
		Map<String, Order> sortKeys = new HashMap<>();
		sortKeys.put("id", Order.ASCENDING);
		H2PagingQueryProvider queryProvider = new H2PagingQueryProvider();
		queryProvider.setSelectClause("select *");
		queryProvider.setFromClause("from client");
		queryProvider.setSortKeys(sortKeys);
		reader.setQueryProvider(queryProvider);
		return reader;
	}

	@Bean
	public ItemWriter<Client> clientItemWriter() {
		JdbcBatchItemWriter<Client> writer = new JdbcBatchItemWriter<>();
        writer.setDataSource(hsds);
        writer.setSql("INSERT INTO CLIENT(ID, ADRESSES1, ADRESSES2, EMAIL, ETATPROVINCE, IDRECOMPENSE, NOMFAMILLE, PRENOM, VILLE, ZIPCODEPOSTAL)"
        		+ "VALUES(:id, :adresses1, :adresses2, :email, :etatProvince, :idRecompense, :nomFamille, :prenom, :ville, :zipCodePostal)");
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Client>());
        return writer;
	}
	@Bean
	public ItemWriter<Genre> genreItemWriter() {
		JdbcBatchItemWriter<Genre> writer = new JdbcBatchItemWriter<>();
        writer.setDataSource(hsds);
        writer.setSql("INSERT INTO GENRE(ID, GENRE) VALUES(:id, :genre)");
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Genre>());
        return writer;
	}
	@Bean
	public ItemWriter<TypeMedia> typeMediaItemWriter() {
		JdbcBatchItemWriter<TypeMedia> writer = new JdbcBatchItemWriter<>();
        writer.setDataSource(hsds);
        writer.setSql("INSERT INTO TYPEMEDIA(ID, MEDIA) VALUES(:id, :media)");
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<TypeMedia>());
        return writer;
	}
	@Bean
	public ItemWriter<Editeur> editeurItemWriter() {
		JdbcBatchItemWriter<Editeur> writer = new JdbcBatchItemWriter<>();
        writer.setDataSource(hsds);
        writer.setSql("INSERT INTO EDITEUR(ID, COURRIEL, NOM, SITEWEB, TELEPHONE) VALUES(:id, :courriel, :nom, :siteWeb, :telephone)");
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Editeur>());
        return writer;
	}

	@Bean
	public Step step1() {
		return stepBuilderFactory.get("step1").<Client, Client>chunk(5).reader(clientReader())
				/*.reader(pagingItemReader())*/.writer(clientItemWriter()).build();
	}
	@Bean
	public Step step2() {
		return stepBuilderFactory.get("step2").<Genre, Genre>chunk(5).reader(genreReader()).writer(genreItemWriter()).build();
	}
	@Bean
	public Step step3() {
		return stepBuilderFactory.get("step3").<TypeMedia, TypeMedia>chunk(5).reader(mediaReader()).writer(typeMediaItemWriter()).build();
	}
	@Bean
	public Step step4() {
		return stepBuilderFactory.get("step4").<Editeur, Editeur>chunk(5).reader(editeurReader()).writer(editeurItemWriter()).build();
	}

	@Bean
	public Flow splitFlow() {
	    return new FlowBuilder<SimpleFlow>("splitFlow")
	        .split(taskExecutor())
	        .add(flow1(), flow2(), flow3(), flow4())
	        .build();
	}
	@Bean
	public Flow flow1() {
	    return new FlowBuilder<SimpleFlow>("flow1")
	        .start(step1())
	        //.next(step2())
	        .build();
	}
	@Bean
	public Flow flow2() {
	    return new FlowBuilder<SimpleFlow>("flow2").start(step2()).build();
	}
	@Bean
	public Flow flow3() {
	    return new FlowBuilder<SimpleFlow>("flow3").start(step3()).build();
	}
	@Bean
	public Flow flow4() {
	    return new FlowBuilder<SimpleFlow>("flow4").start(step4()).build();
	}
	@Bean
	public TaskExecutor taskExecutor() {
	    return new SimpleAsyncTaskExecutor("spring_batch");
	}
	
	@Bean
	public Job job() {
		return jobBuilderFactory.get("job").start(splitFlow()).build().build();
	}
}
