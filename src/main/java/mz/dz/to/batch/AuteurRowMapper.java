package mz.dz.to.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mz.dz.to.entities.Auteur;

public class AuteurRowMapper implements RowMapper<Auteur> {

	@Override
	public Auteur mapRow(ResultSet rs, int rowNum) throws SQLException {
		Auteur a = new Auteur();
		a.setId(rs.getLong("id"));
		a.setNomFamille(rs.getString("NOMFAMILLE"));
		a.setPrenom(rs.getString("Prenom"));
		//a.setLivres(null);
		return a;
	}
}
