package mz.dz.to.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mz.dz.to.entities.Client;

public class ClientRowMapper implements RowMapper<Client> {
	
	@Override
	public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
		Client c = new Client();
		c.setId(rs.getLong("id"));
		c.setAdresses1(rs.getString("Adresses1"));
		c.setAdresses2(rs.getString("Adresses2"));
		c.setEmail(rs.getString("Email"));
		c.setEtatProvince(rs.getString("EtatProvince"));
		c.setIdRecompense(rs.getInt("IdRecompense"));
		c.setNomFamille(rs.getString("NomFamille"));
		c.setPrenom(rs.getString("Prenom"));
		c.setVille(rs.getString("Ville"));
		c.setZipCodePostal(rs.getString("ZipCodePostal"));
		return c; 
	}
}
