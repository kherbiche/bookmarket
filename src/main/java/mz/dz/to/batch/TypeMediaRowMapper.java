package mz.dz.to.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mz.dz.to.entities.TypeMedia;

public class TypeMediaRowMapper implements RowMapper<TypeMedia> {

	@Override
	public TypeMedia mapRow(ResultSet rs, int rowNum) throws SQLException {
		TypeMedia tm = new TypeMedia();
		tm.setId(rs.getLong("id"));
		tm.setMedia(rs.getString("Media"));
		return tm;
	}

}
