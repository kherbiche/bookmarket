package mz.dz.to.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mz.dz.to.entities.Editeur;

public class EditeurRowMapper implements RowMapper<Editeur> {

	@Override
	public Editeur mapRow(ResultSet rs, int rowNum) throws SQLException {
		Editeur e = new Editeur();
		e.setId(rs.getLong("id"));
		e.setNom(rs.getString("nom"));
		e.setSiteWeb(rs.getString("siteWeb"));
		e.setTelephone(rs.getString("telephone"));
		e.setCourriel(rs.getString("courriel"));
		return e;
	}

}
