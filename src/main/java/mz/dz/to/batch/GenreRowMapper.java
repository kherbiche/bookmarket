package mz.dz.to.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mz.dz.to.entities.Genre;

public class GenreRowMapper implements RowMapper<Genre> {

	@Override
	public Genre mapRow(ResultSet rs, int rowNum) throws SQLException {
		Genre g = new Genre();
		g.setId(rs.getLong("id"));
		g.setGenre(rs.getString("genre"));
		return g;
	}

}
