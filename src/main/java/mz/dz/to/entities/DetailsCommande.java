package mz.dz.to.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class DetailsCommande {
	@EmbeddedId
	private DetailsCommandeId id;
	@ManyToOne
	@MapsId("id_commande")
	private Commande commande;
	@ManyToOne
	@MapsId("id_livre")
	private Livre livre;
	private String quantite;
	private String prixUnitaire;
	private String montantRabais;
	@ManyToOne
	@JoinColumn(name = "fk_typeMedia")
	private TypeMedia typeMedia;
	
	public DetailsCommande() {id = new DetailsCommandeId();}
	
	public DetailsCommandeId getId() {
		return id;
	}
	public Commande getCommande() {
		return commande;
	}
	public Livre getLivre() {
		return livre;
	}
	public String getQuantite() {
		return quantite;
	}
	public String getPrixUnitaire() {
		return prixUnitaire;
	}
	public String getMontantRabais() {
		return montantRabais;
	}
	public TypeMedia getTypeMedia() {
		return typeMedia;
	}
	public void setId(DetailsCommandeId id) {
		this.id = id;
	}
	public void setCommande(Commande commande) {
		this.commande = commande;
	}
	public void setLivre(Livre livre) {
		this.livre = livre;
	}
	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}
	public void setPrixUnitaire(String prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	public void setMontantRabais(String montantRabais) {
		this.montantRabais = montantRabais;
	}
	public void setTypeMedia(TypeMedia typeMedia) {
		this.typeMedia = typeMedia;
	}
}
