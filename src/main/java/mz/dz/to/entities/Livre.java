package mz.dz.to.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name="LIVRE") 
@Entity
public class Livre {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	private String isbn;
	private String titre;
	private String prix;
	private String datePublication;
	@ManyToOne
	@JoinColumn(name = "fk_editeur")
	private Editeur editeur;
	@ManyToOne
	@JoinColumn(name = "fk_genre")
	private Genre genre;
	@ManyToMany(mappedBy="livres")
	private List<Auteur> auteurs;
	
	public Livre() {}

	public long getId() {
		return id;
	}
	public String getIsbn() {
		return isbn;
	}
	public String getTitre() {
		return titre;
	}
	public String getPrix() {
		return prix;
	}
	public String getDatePublication() {
		return datePublication;
	}
	public Editeur getEditeur() {
		return editeur;
	}
	public Genre getGenre() {
		return genre;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public void setPrix(String prix) {
		this.prix = prix;
	}
	public void setDatePublication(String datePublication) {
		this.datePublication = datePublication;
	}
	public void setEditeur(Editeur editeur) {
		this.editeur = editeur;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	public List<Auteur> getAuteurs() {
		return auteurs;
	}
	public void setAuteurs(List<Auteur> auteurs) {
		this.auteurs = auteurs;
	}
	
}
