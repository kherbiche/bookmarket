package mz.dz.to.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

@Table(name="CLIENT") 
@Entity
public class Client {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	private String nomFamille;
	private String prenom;
	private String adresses1;
	private String adresses2;
	private String ville;
	private String etatProvince;
	private String zipCodePostal;
	private String email;
	private long idRecompense;
	
	public Client() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomFamille() {
		return nomFamille;
	}

	public void setNomFamille(String nomFamille) {
		this.nomFamille = nomFamille;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresses1() {
		return adresses1;
	}

	public void setAdresses1(String adresses1) {
		this.adresses1 = adresses1;
	}

	public String getAdresses2() {
		return adresses2;
	}

	public void setAdresses2(String adresses2) {
		this.adresses2 = adresses2;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getEtatProvince() {
		return etatProvince;
	}

	public void setEtatProvince(String etatProvince) {
		this.etatProvince = etatProvince;
	}

	public String getZipCodePostal() {
		return zipCodePostal;
	}

	public void setZipCodePostal(String zipCodePostal) {
		this.zipCodePostal = zipCodePostal;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getIdRecompense() {
		return idRecompense;
	}

	public void setIdRecompense(long idRecompense) {
		this.idRecompense = idRecompense;
	}
}
