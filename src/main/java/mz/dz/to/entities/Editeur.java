package mz.dz.to.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="EDITEUR") 
@Entity
public class Editeur {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	private String nom;
	private String telephone;
	private String courriel;
	private String siteWeb;

	public Editeur() {}
	public long getId() {
		return id;
	}
	public String getNom() {
		return nom;
	}
	public String getTelephone() {
		return telephone;
	}
	public String getCourriel() {
		return courriel;
	}
	public String getSiteWeb() {
		return siteWeb;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public void setCourriel(String courriel) {
		this.courriel = courriel;
	}
	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}
}
