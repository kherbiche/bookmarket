package mz.dz.to.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="GENRE") 
@Entity
public class Genre {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	private String genre;
	
	public Genre() {}

	public long getId() {
		return id;
	}
	public String getGenre() {
		return genre;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
}
