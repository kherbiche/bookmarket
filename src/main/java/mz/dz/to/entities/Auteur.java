package mz.dz.to.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Table(name="AUTEUR") 
@Entity
public class Auteur {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	private String nomFamille;
	private String prenom;
	@ManyToMany
	@JoinTable (
		       name="LivreAuteur",
		       joinColumns = {@JoinColumn(name="fk_auteur")},
		       inverseJoinColumns = {@JoinColumn(name="fk_livre")}
				)
	private List<Livre> livres;
	
	public Auteur() {}

	public long getId() {
		return id;
	}
	public String getNomFamille() {
		return nomFamille;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setNomFamille(String nomFamille) {
		this.nomFamille = nomFamille;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public List<Livre> getLivres() {
		return livres;
	}
	public void setLivres(List<Livre> livres) {
		this.livres = livres;
	}
}
