package mz.dz.to.entities;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class DetailsCommandeId implements java.io.Serializable {
	@Transient
	private static final long serialVersionUID = 1L;
	//@Column(name="IDCOMMANDE")
	private long id_commande;
	//@Column(name="IDLIVRE")
	private long id_livre;
	
	public DetailsCommandeId() {}
	public long getId_commande() {return id_commande;}
	public long getId_livre() {return id_livre;}
	public void setId_commande(long id_commande) {this.id_commande = id_commande;}
	public void setId_livre(long id_livre) {this.id_livre = id_livre;}
}
