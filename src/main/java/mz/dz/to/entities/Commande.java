package mz.dz.to.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="COMMANDE")
public class Commande {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	@ManyToOne
	@JoinColumn(name = "fk_client")
	private Client client;
	private String dateCommande;
	private String dateLivraison;
	private String livrerAdresse1;
	private String livrerAdresse2;
	private String livrerVille;
	private String livrerProvince;
	private String livrerZip;
	
	public Commande() {}

	public long getId() {
		return id;
	}
	public String getDateCommande() {
		return dateCommande;
	}
	public String getDateLivraison() {
		return dateLivraison;
	}
	public String getLivrerAdresse1() {
		return livrerAdresse1;
	}
	public String getLivrerAdresse2() {
		return livrerAdresse2;
	}
	public String getLivrerVille() {
		return livrerVille;
	}
	public String getLivrerProvince() {
		return livrerProvince;
	}
	public String getLivrerZip() {
		return livrerZip;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}

	public void setDateCommande(String dateCommande) {
		this.dateCommande = dateCommande;
	}
	public void setDateLivraison(String dateLivraison) {
		this.dateLivraison = dateLivraison;
	}
	public void setLivrerAdresse1(String livrerAdresse1) {
		this.livrerAdresse1 = livrerAdresse1;
	}
	public void setLivrerAdresse2(String livrerAdresse2) {
		this.livrerAdresse2 = livrerAdresse2;
	}
	public void setLivrerVille(String livrerVille) {
		this.livrerVille = livrerVille;
	}
	public void setLivrerProvince(String livrerProvince) {
		this.livrerProvince = livrerProvince;
	}
	public void setLivrerZip(String livrerZip) {
		this.livrerZip = livrerZip;
	}
	
}
