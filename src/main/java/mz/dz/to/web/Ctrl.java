package mz.dz.to.web;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

import mz.dz.to.entities.Client;
import mz.dz.to.service.SeltanMejbada;

@RestController
@RequestMapping(value = "/benboulaid")
public class Ctrl {
	private static final Logger logger = LoggerFactory.getLogger(Ctrl.class);
	@Autowired
	private SeltanMejbada service ;
	
	@GetMapping("/client")
	public void testMyWork() {
		logger.info("uri: /client/");
		Client c = new Client();
		c.setNomFamille("nomFami1");
		service.createClient(c);
	}
	//curl -i -X POST localhost:8081/benboulaid/pop -H "Content-Type: text/json" --data-binary "@/home/micipsa/Téléchargements/Data.txt"
	@PostMapping("/pop")
	public void populate(@RequestBody com.fasterxml.jackson.databind.JsonNode str) {
		logger.info("uri: /pop \n" + str);
		CtrlHelper helper = new CtrlHelper();
		Iterator<JsonNode> itr = str.elements();
		while(itr.hasNext()) {
			helper.parsing(itr.next().get("Data"));
			service.process(helper);
		}
	}
}
