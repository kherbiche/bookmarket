package mz.dz.to.web;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

import mz.dz.to.entities.Auteur;
import mz.dz.to.entities.Client;
import mz.dz.to.entities.Commande;
import mz.dz.to.entities.DetailsCommande;
import mz.dz.to.entities.Editeur;
import mz.dz.to.entities.Genre;
import mz.dz.to.entities.Livre;
import mz.dz.to.entities.TypeMedia;

public class CtrlHelper {
	private static final Logger logger = LoggerFactory.getLogger(CtrlHelper.class);
	private Livre livre;
	private Editeur editeur;
	private Auteur auteur;
	private Genre genre;
	private Commande commande;
	private DetailsCommande detailsCommande;
	private Client client;
	private TypeMedia typeMedia;

	public void parsing(JsonNode jn) {
		
		logger.info("is Array: "+jn.isArray());
		logger.info("is ContainerNode: "+jn.isContainerNode());
		logger.info("is Object: "+jn.isObject());
		logger.info("jn: "+jn);
		//logger.info("Data: "+jn.get("Data"));
		 /*
		 * curl -X POST localhost:8081/benboulaid/pop -H "Content-Type:application/json" -d ''
		 */
		livre = new Livre();
		editeur = new Editeur();
		auteur = new Auteur();
		genre = new Genre();
		commande = new Commande();
		detailsCommande = new DetailsCommande();
		client = new Client();
		typeMedia = new TypeMedia();

		livre.setIsbn(jn.get("ISBN").asText());
		livre.setTitre(jn.get("Titre").asText());
		livre.setPrix(jn.get("Prix").asText());
		livre.setDatePublication(jn.get("DatePublication").asText());

		editeur.setSiteWeb(jn.get("SiteWeb").asText());

		auteur.setPrenom(jn.get("Auteurs").get("Prenom").asText());
		auteur.setNomFamille(jn.get("Auteurs").get("Nom").asText());
		///////
		// livre.setAuteurs(new ArrayList<Auteur>(){{add(auteur);}});
		//////
		livre.setAuteurs(Stream.of(auteur).collect(Collectors.toList()));
		auteur.setLivres(Arrays.asList(livre));
		
		////////////
		// logger.info("Genre: "+jn.get("Genre"));
		// logger.info("Genre: "+jn.get("Genre").iterator().next().asText());
		////////////
		genre.setGenre(jn.get("Genre").iterator().next().asText());
		livre.setGenre(genre);

		commande.setDateCommande(jn.get("Commandes").get("DateCommande").asText());
		commande.setDateLivraison(jn.get("Commandes").get("DateLivraison").asText());
		commande.setLivrerAdresse1(jn.get("Commandes").get("LivrerAdresse1").asText());
		commande.setLivrerAdresse2(jn.get("Commandes").get("LivrerAdresse2").asText());
		commande.setLivrerVille(jn.get("Commandes").get("LivrerVille").asText());
		commande.setLivrerProvince(jn.get("Commandes").get("LivrerProvince").asText());
		commande.setLivrerZip(jn.get("Commandes").get("LivrerZip").asText());
		
		client.setZipCodePostal(jn.get("Commandes").get("ZipCodePostal").asText());
		
		typeMedia.setMedia(jn.get("DetailCommande").get("Media").asText());
		
		detailsCommande.setCommande(commande);
		detailsCommande.setLivre(livre);
		detailsCommande.setPrixUnitaire(jn.get("DetailCommande").get("PrixUnitaire").asText());
		detailsCommande.setQuantite(jn.get("DetailCommande").get("Quantite").asText());
		detailsCommande.setMontantRabais(jn.get("DetailCommande").get("MontantRabais").asText());
		detailsCommande.setTypeMedia(typeMedia);
	}
	public Livre getLivre() {return livre;}
	public Editeur getEditeur() {return editeur;}
	public Auteur getAuteur() {return auteur;}
	public Genre getGenre() {return genre;}
	public Commande getCommande() {return commande;}
	public DetailsCommande getDetailsCommande() {return detailsCommande;}
	public Client getClient() {return client;}
	public TypeMedia getTypeMedia() {return typeMedia;}
}
